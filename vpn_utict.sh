#!/bin/bash

echo "Empezamos..."

netstat -rn

# Hay que correr ifconfig utunX

cadena=`ifconfig $1`
re='([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})'
if [[ $cadena =~ $re ]]; then GW=${BASH_REMATCH[1]}; fi

echo $GW

sudo route delete -net -ifscope utun4 0.0.0.0 $GW
echo "__________________________________________________________________"
sudo route -n delete 128.0/1
echo "__________________________________________________________________"
sudo route -vn add 192.168.0.0 $GW
echo "__________________________________________________________________"
echo "Fin"

# En mac el comando para agregar una ruta es este
# $ route -n add 10.0.0.0/24 10.0.0.1
# En linux es éste
# $ route -n add -net 10.0.0.0/24 gw 10.0.0.1
